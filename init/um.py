#!../venv/bin/python3

import os
import sys

__filepath__ = os.path.abspath(__file__)
PWD = os.path.dirname(__filepath__) + '/../'
sys.path.append(PWD) # path-hack

PATH_root = PWD + '../'
sys.path.append(PATH_root) # path-hack

import pwd
import grp
import datetime
import random

import sqlalchemy as sa

from pysrv import db
import pysrv.db.passwd as db_passwd

def gen_Users():
    for row in pwd.getpwall():
        if len(row.pw_name) > 10: continue
        if row.pw_uid == 0: continue # skip root
        user = db_passwd.Users(
            uid       = row.pw_uid,
            uname     = row.pw_name,
            fullname  = row.pw_gecos,
            groups    = 100,
            isEnabled = bool(random.getrandbits(1)),
            createdOn = datetime.datetime.now(),
            updatedOn = datetime.datetime.now(),
            validity  = datetime.datetime(
                random.randint(2017,2019),
                random.randint(1,12),
                random.randint(1,28)
            ).date(),
            card      = row.pw_uid+1
        )

        session.add(user)


    session.add(db_passwd.Users(
        uid       = 0,
        card      = 1,
        uname     = 'root',
        fullname  = 'Super User',
        groups    = 0,
        isEnabled  = True,
        createdOn = datetime.datetime(1000, 1, 1),
        updatedOn = datetime.datetime(9000, 1, 1),
        validity  = datetime.datetime(
            random.randint(2017,2019),
            random.randint(1,12),
            random.randint(1,28)
        ).date()
    ))

    session.flush()


def gen_Groups():
    for row in grp.getgrall():
        if not len(row.gr_mem): continue
        if row.gr_gid in (0, 100): continue # skip already added
        group = db_passwd.Groups(
            gid       = row.gr_gid,
            gname     = row.gr_name,
            createdOn = datetime.datetime.now(),
            updatedOn = datetime.datetime.now(),
            dailyStart=datetime.datetime(1, 1, 1, 9, 30).time(),
            dailyEnd=datetime.datetime(1, 1, 1, 17, 30).time(),
            validity  = datetime.datetime(
                random.randint(2017,2019),
                random.randint(1,12),
                random.randint(1,28)
            ).date(),
            weekly    = str(random.randint(0,123456))
        )
        session.add(group)

    session.add(db_passwd.Groups(
        gid         = 0,
        gname       = 'root',
        description = "Super Group",
        createdOn   = datetime.datetime(1000, 1, 1),
        updatedOn   = datetime.datetime(9000, 1, 1),
        dailyStart  = datetime.datetime(1,1,1,9,30).time(),
        dailyEnd    = datetime.datetime(1, 1, 1, 17, 30).time(),
        validity    = datetime.datetime(
            random.randint(2017,2019),
            random.randint(1,12),
            random.randint(1,28)
        ).date(),
        weekly      = '0123456'
    ))

    session.add(db_passwd.Groups(
        gid         = 100,
        gname       = 'users',
        description = 'Weekdays users',
        dailyStart  = datetime.datetime(1, 1, 1, 9, 30).time(),
        dailyEnd    = datetime.datetime(1, 1, 1, 17, 30).time(),
        createdOn   = datetime.datetime(1000, 1, 1),
        updatedOn   = datetime.datetime(9000, 1, 1),
        validity    = datetime.datetime(
            random.randint(2017,2019),
            random.randint(1,12),
            random.randint(1,28)
        ).date(),
        weekly      = '01234'
    ))

    session.flush()


if __name__ == '__main__':
    engine  = sa.create_engine(
        db.DATABASE_PATH,
        echo = True
    )

    db.Base.metadata.drop_all(bind=engine, checkfirst=True)
    db.Base.metadata.create_all(bind=engine)

    Session = sa.orm.sessionmaker(bind=engine)
    session = Session()

    gen_Groups()
    gen_Users()
    session.commit()
