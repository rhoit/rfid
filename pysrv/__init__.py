import os
import datetime

__filepath__= os.path.abspath(__file__)
PWD = os.path.dirname(__filepath__) + '/'

import sqlalchemy as sa
from pysrv import db

engine = sa.create_engine(db.DATABASE_PATH, echo=True)
Session = sa.orm.sessionmaker(bind=engine)
session = Session()

import flask
app = flask.Flask(__name__)
app.secret_key = 'my unobvious secret key'


@app.route('/vendors/<path:filepath>')
def serve_vendor(filepath):
    return flask.send_from_directory('../vendors', filepath)


@app.route('/static/<path:filepath>')
def serve_static(filename):
    return flask.url_for('static', filename)



# * CustomJSONEncoder
#----------------------------------------------------------------------

class CustomJSONEncoder(flask.json.JSONEncoder):
    def default(self, obj):
        if   isinstance(obj, datetime.date): return obj.isoformat()
        try:
            iterable = iter(obj)
        except TypeError:
            pass
        else:
            return tuple(iterable)

        return flask.json.JSONEncoder.default(self, obj)

app.json_encoder = CustomJSONEncoder



# * CustomResponse
#----------------------------------------------------------------------

class CustomResponse(flask.Response):
    # charset = 'utf16'
    @classmethod
    def force_type(cls, rv, environ=None):
        if isinstance(rv, dict):
            rv = flask.jsonify(rv)
        return super().force_type(rv, environ)

app.response_class = CustomResponse



# * BLUEPRINTS
#----------------------------------------------------------------------

api = flask.Blueprint('api', __name__)
dash = flask.Blueprint('dash', __name__)

import pysrv.ctrl
import pysrv.views
