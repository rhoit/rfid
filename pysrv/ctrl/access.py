#!/usr/bin/env python3
from sqlalchemy import exc
import datetime as d

import pysrv
from pysrv import api
import  pysrv.db.access as db_access


@api.route('/logs')
def api_data_logs():
    results = pysrv.session.execute(
        """
        SELECT AccessLogs.card, AccessLogs.uid, uname, timestamp, isSuccess
        FROM AccessLogs
        LEFT OUTER JOIN Users
        ON AccessLogs.uid = Users.uid;
    """)

    return {
        'type'    : "array",
        'headers' : results.keys(),
        'entries' : results.rowcount,
        'data'    : results
    }


@api.route('/card/<int:card>', methods=['GET'])
def api_card(card):
    msg,isSuccess,uid = db_access.userAccess(card)
    log = db_access.AccessLogs(
        timestamp = d.datetime.now(),
        card      = card,
        uid       = uid,
        isSuccess = isSuccess
    )

    pysrv.session.add(log)

    try:
        pysrv.session.commit()

    except exc.SQLAlchemyError as e:
        pysrv.session.rollback()
        return str(e.__cause__)

    return msg
