#!/usr/bin/env python3
import datetime

from flask import request
from sqlalchemy import exc

import pysrv
from pysrv import api
import  pysrv.db.passwd as db_passwd


@api.route('/groups')
def api_groups_lst():
    results = pysrv.session.execute("""
        SELECT gid, gname, count(uid) 'users'
        FROM "Groups"
        LEFT OUTER JOIN "Users"
        ON "Groups".gid = "Users".groups
        GROUP BY gid
    """)

    return {
        'type'    : "array",
        'headers' : results.keys(),
        'entries' : results.rowcount,
        'data'    : results
    }


@api.route('/groups/add', methods=['POST'])
def api_group_add():
    data = request.form
    if data is None:
        return 'No data for registration', 405

    kvdata = { k:v for k, v in data.items() }

    kvdata['validity']   = datetime.datetime.strptime(
        data.get('validity'),
        '%Y-%m-%d'
    ).date()
    kvdata['weekly']     = ''.join(
        data.getlist('weekly')
    )
    kvdata['dailyStart'] = datetime.datetime.strptime(
        data.get('dailyStart')[:5],
        '%H:%M'
    ).time()
    kvdata['dailyEnd']   = datetime.datetime.strptime(
        data.get('dailyEnd')[:5],
        '%H:%M'
    ).time()

    print (kvdata)
    group=db_passwd.Groups(
        **kvdata,
        createdOn = datetime.datetime.now(),
        updatedOn = datetime.datetime.now()

    )

    try:
        pysrv.session.add(group)
        pysrv.session.commit()
        return "Group Added"

    except exc.IntegrityError:
        pysrv.session.rollback()
        return "ERROR: Group already exist!", 403

    except exc.SQLAlchemyError as e:
        pysrv.session.rollback()
        return str(e.__cause__), 405


@api.route('/groups/<int:gid>', methods=['DELETE'])
def api_group_delete(gid):
    try:
        pysrv.session.query(
            db_passwd.Groups
        ).filter(
            db_passwd.Groups.gid == gid
        ).delete()
        pysrv.session.commit()
        return "Deleted"

    except exc.SQLAlchemyError as e:
        pysrv.session.rollback()
        return str(e.__cause__)


@api.route('/groups/<int:gid>', methods=['PATCH'])
def api_group_patch(gid):
    data = request.form
    if data is None:
        return 'No data for registration', 405

    group = pysrv.session.query(
               db_passwd.Groups
               ).filter(
               db_passwd.Groups.gid == gid
               ).first()

    group.gname       = data.get('gname')
    group.description = data.get('description')
    group.validity    = datetime.datetime.strptime(
        data.get('validity'),
        '%Y-%m-%d'
    ).date()
    group.weekly      = ''.join(
        data.getlist('weekly')
    )
    group.dailyStart  = datetime.datetime.strptime(
        data.get('dailyStart')[:5],
        '%H:%M'
    ).time()
    group.dailyEnd    = datetime.datetime.strptime(
        data.get('dailyEnd')[:5],
        '%H:%M'
    ).time()
    group.updatedOn   = datetime.datetime.now()

    try:
        pysrv.session.commit()
        return "Group updated."

    except exc.IntegrityError as e:
        pysrv.session.rollback()
        return str(e.__cause__), 403

    except exc.SQLAlchemyError as e:
        pysrv.session.rollback()
        return str(e.__cause__)
