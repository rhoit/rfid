#!/usr/bin/env python3

import random
import datetime

from flask import request
from sqlalchemy import exc

import  pysrv
from    pysrv import api
import  pysrv.db.passwd as db_passwd


@api.route('/users')
def api_users_lst():
    results = pysrv.session.execute(
        'SELECT uid, uname, isEnabled FROM "Users"'
    )
    return {
        'type'    : "array",
        'headers' : results.keys(),
        'entries' : results.rowcount,
        'data'    : results
    }


@api.route('/users/add', methods=['POST'])
def api_user_add():
    data = request.form
    if data is None:
        return 'No data for registration', 405

    kvdata = { k:v for k, v in data.items()}
    kvdata['validity'] = datetime.datetime.strptime(
        data.get('validity'), '%Y-%m-%d').date()
    user=db_passwd.Users(
        **kvdata,
        isEnabled  = bool(random.getrandbits(1)),
        createdOn = datetime.datetime.now(),
        updatedOn = datetime.datetime.now()
    )

    try:
        pysrv.session.add(user)
        pysrv.session.commit()
        return "User Added"

    except exc.IntegrityError:
        pysrv.session.rollback()
        return "ERROR: Username already exist!"

    except exc.SQLAlchemyError as e:
        pysrv.session.rollback()
        return str(e.__cause__)


@api.route('/users/<int:uid>', methods=['DELETE'])
def api_user_delete(uid):
    try:
        pysrv.session.query(
            db_passwd.Users
        ).filter(
            db_passwd.Users.uid == uid
        ).delete()
        pysrv.session.commit()
        return "Deleted"

    except exc.SQLAlchemyError as e:
        pysrv.session.rollback()
        return str(e.__cause__)


@api.route('/users/<int:uid>', methods=['PATCH'])
def api_user_patch(uid):
    data = request.form
    if data is None:
        return 'No data for registration', 405

    user = pysrv.session.query(
               db_passwd.Users
               ).filter(
               db_passwd.Users.uid == uid
               ).one()

    for k, v in data.items():
        if not hasattr(user, k): continue
        setattr(user, k, v)

    isEnabled = data.get('isEnabled')
    if isEnabled is not None:
        user.isEnabled = isEnabled == "true"

    validity = data.get('validity')
    if validity is not None:
        user.validity = datetime.datetime.strptime(
            validity,'%Y-%m-%d').date()

    user.updatedOn = datetime.datetime.now()
    try:
        pysrv.session.commit()
        return "user updated"
    except exc.IntegrityError as e:
        pysrv.session.rollback()
        return str(e.__cause__), 403
    except exc.SQLAlchemyError as e:
        pysrv.session.rollback()
        return str(e.__cause__)
