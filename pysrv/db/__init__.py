import os

__filepath__= os.path.abspath(__file__)
PWD = os.path.dirname(__filepath__) + '/../../'

DATABASE_PATH ='sqlite:///{0}rfid.db'.format(PWD)

import sqlalchemy as sa
import sqlalchemy.ext.declarative


Base = sa.ext.declarative.declarative_base()
