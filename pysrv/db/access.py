import sqlalchemy as sa
import datetime

from pysrv.db import Base

import pysrv
from pysrv.db import passwd as db_passwd

class AccessLogs(Base):
    __tablename__ = "AccessLogs"

    timestamp     = sa.Column(sa.DateTime(timezone=True),primary_key=True)
    card          = sa.Column(sa.String(100))
    uid           = sa.Column(sa.Integer)
    isSuccess     = sa.Column(sa.Boolean,nullable=False)

def userAccess(card):
    user      = pysrv.session.query(
        db_passwd.Users
    ).filter(
        db_passwd.Users.card == card
    ).first()

    if  user is None:
        return "Unknown card.", False, None

    if not user.isEnabled:
        return "User is Disabled!!", False, user.uid

    now = datetime.datetime.today()
    if not (now.date() <= user.validity):
        return "User validity expired!!", False, user.uid

    group = pysrv.session.query(
        db_passwd.Groups
    ).filter(
        db_passwd.Groups.gid == user.groups
    ).first()


    if not (now.date() <= group.validity):
        return "Group validity expired!!", False, user.uid

    if  not str(datetime.datetime.today().weekday()) in group.weekly:
        return "Not allowed Today!", False, user.uid


    if  not (now.time() >= group.dailyStart and now.time() <= group.dailyEnd):
        return "Not allowed now!!", False, user.uid

    user.lastIn = now
    return "Please get in!!", True, user.uid
