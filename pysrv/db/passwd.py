import sqlalchemy as sa

from pysrv.db.access import Base

class Users(Base):
    __tablename__ = "Users"

    uid           = sa.Column(sa.Integer, autoincrement=True, primary_key=True)

    uname         = sa.Column(sa.String(10), unique=True)
    fullname      = sa.Column(sa.String(100))
    card          = sa.Column(sa.Integer, unique=True, nullable=True)

    groups        = sa.Column(sa.Integer, default=100, nullable=False)

    validity      = sa.Column(sa.Date)
    isEnabled      = sa.Column(sa.Boolean, default=False)
    lastIn        = sa.Column(sa.DateTime(timezone=True))
    createdOn     = sa.Column(sa.DateTime(timezone=True))
    updatedOn     = sa.Column(sa.DateTime(timezone=True))

class Groups(Base):
    __tablename__ = "Groups"

    gid           = sa.Column(sa.Integer, autoincrement=True, primary_key=True)
    gname         = sa.Column(sa.String(10), unique=True)

    description   = sa.Column(sa.String(20))

    validity      = sa.Column(sa.Date)

    dailyStart    = sa.Column(sa.Time(timezone=True))
    dailyEnd      = sa.Column(sa.Time(timezone=True))
    weekly        = sa.Column(sa.String(7)) # M=0 T=1 w=2 T=3 F=4 S=5 S=6
    createdOn     = sa.Column(sa.DateTime(timezone=True))
    updatedOn     = sa.Column(sa.DateTime(timezone=True))
