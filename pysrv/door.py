#!/usr/bin/env python3

import RPi.GPIO as GPIO
import time

class doortrigger(object):
    def __init__(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setwarnings(False)
        GPIO.setup(11,GPIO.OUT)
        GPIO.output(11,GPIO.LOW)


    def unlock_door(self):
        GPIO.output(11, GPIO.HIGH)
        GPIO.output(11, GPIO.LOW)
