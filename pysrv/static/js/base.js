function toggle_menu(e) {
    $("#sidebar-wrapper").toggleClass("toggled");
    $('.overlay').fadeToggle()
}

function toggle_search(e) {
$('#toggle_search').prop('disabled', function(i, v) { return !v; }).focus();
}

function go_back() {
        window.history.go(-1);
        return false;
}

function select_multiple() {
    var self = $(this)
    var parent = self.parent()
    var originalScrollTop = self.parent().scrollTop();
    parent.focus()
    self.prop("selected", ! self.prop("selected"))
    setTimeout(function() {
        parent.scrollTop(originalScrollTop);
    }, 0)
    return false
}
