function optsGen(json) {
    var opts = {
        //'stateSave'      : true,
        'retrieve'       : true,
        'paging'         : false,
        'dom'            : "it",
        'deferRender'    : true,
        'columnDefs'     : [],
        'fixedHeader'    : {
            'headerOffset': $('#navMenu').outerHeight()
        },
        'language'       : {
            'decimal'    : ".",
            'thousands'  : ","
        },
    }
    opts.iMap = {}
    opts.visible = function(dt, key) {
        icol = dt.api().data().column(this.iMap[key]).index('visible')
        if(typeof icol == "number") return icol
    }
    if (! json) return opts
    opts['data'] = json.data
    return opts
}


function ajax_reload_dt(tid, url) {
    var xhr = new XMLHttpRequest()
    xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var table = $(tid).DataTable()
            table.clear()
            table.rows.add(JSON.parse(this.responseText).data)
            table.draw()
        }
    }
    xhr.open("GET", url, true)
    xhr.send()
}


function json_create_dt(tid, json) {
    var opts = optsGen(json)
    $.each(json.headers, function(i, val) {
        opts.iMap[val] = i
        $(tid+' thead tr').append('<th>'+val+'</th>')
    })
    return $(tid).DataTable(opts)
}


function ajax_create_dt(tid, url, callback) {
    var xhr = new XMLHttpRequest()
    xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var jObj = JSON.parse(this.responseText)
            json_create_dt(tid, jObj)
            if (typeof callback == 'function') callback(jObj)
            return jObj
        } else if (this.readyState == 4 && this.status >= 400) {
            alert(xhr.status + ": " + xhr.responseText)
        }
    }
    xhr.open("GET", url, true)
    xhr.send()
}


function ajax_childRow(table, tr, ikey, apiRoot) {
    var info = table.row(tr).data()
    var row  = table.row(tr)

    if(row.child.isShown()) {
        row.child.hide()
        tr.classList.remove('table-info')
        return
    }

    $.ajax({
        'url'     : apiRoot + info[ikey] + '?xhr=True',
        'success' : function(data) {
            row.child(data).show()
            tr.classList.add('table-info')
        },
        'error'   : function(xhr, status, error) {
            alert(xhr.status + ': ' + xhr.responseText)
        }
    })
}
