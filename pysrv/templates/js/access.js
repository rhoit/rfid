function json_create_dt(tid, json) {
    var opts = optsGen(json)
    var iMap = opts.iMap

    $.each(json.headers, function(i, val) {
        iMap[val] = i
        $(tid+' thead tr').append('<th>'+val+'</th>')

        switch(val) {

            case 'uid':
                opts.columnDefs.push({
                    'targets'    : i,
                    'visible'    : false,
                    'searchable' : false,
                })
                break
        }
    })

    opts.order = [[ iMap['timestamp'], "desc" ]]

    opts.createdRow = function(row, data, index) {

        var icon = data[iMap.isSuccess] ? 'fa-check' : 'fa-times'
        $('td', row).eq(opts.visible(this, 'isSuccess')).html(
            '<i class="fas '+ icon +'"></i>'
        )

        if(data[iMap.timestamp] != null) {
            var timestamp = new Date(data[iMap.timestamp])
            $('td', row).eq(opts.visible(this, 'timestamp')).text(
                timestamp.toString().substring(0, 24)
            )
        }

        if(data[iMap.lastIn] != null) {
            var lastIn = new Date(data[iMap.lastIn])
            $('td', row).eq(opts.visible(this, 'lastIn')).text(
                lastIn.toString().substring(0, 24)
            )

        }
    }

    var table = $(tid).DataTable(opts)
    $(tid + ' tbody').on('dblclick', "tr[role='row']", function () {
        if (table.row(this).data()[1] != null) {
            ajax_childRow(table, this, iMap.uid, '/users/')
        }
        else{
            alert("Unknown card.")
        }
    })

}
