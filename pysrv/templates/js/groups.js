function json_create_dt(tid, json) {
    var opts = optsGen(json)
    var iMap = opts.iMap

    $.each(json.headers, function(i, val) {
        iMap[val] = i
        $(tid+' thead tr').append('<th>'+val+'</th>')
    })

    opts.order = [[ iMap['users'], "desc" ]]

    table = $(tid).DataTable(opts)
    $(tid + ' tbody').on('dblclick', 'tr', function() {
        ajax_childRow(table, this, iMap.gid, '/groups/')
    })
}

function groupDelete(gid,element){
    var decision = confirm("Are you sure ?");
    if (!decision) return

    $.ajax({
        'url': '/api/groups/'+gid,
        'type': 'DELETE',
        'success': function(data){
            var tr  = $(element).parent().parent().prev()
            var dt  = $("#mytable").DataTable()
            var row = dt.row(tr)
            row.remove().draw()
            alert(data)
        }
    })
}
