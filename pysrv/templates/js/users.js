function json_create_dt(tid, json) {
    var opts = optsGen(json)
    var iMap = opts.iMap

    $.each(json.headers, function(i, val) {
        iMap[val] = i
        $(tid+' thead tr').append('<th>'+val+'</th>')
    })

    opts.order = [[iMap['isEnabled'], "desc"], [ iMap['uid'], "asc" ]]
    opts.rowCallback = function(row, data, index) {
        var icon = data[iMap.isEnabled] ? 'fa-check-circle' : 'fa-circle'
        $('td', row).eq(iMap.isEnabled).html(
            '<i class="fa '+ icon +'" onClick="toggleActive('+data[iMap.uid]+', this)"></i>'
        )
    }


    var table = $(tid).DataTable(opts)
    $(tid + ' tbody').on('dblclick', 'tr', function() {
        ajax_childRow(table, this, iMap.uid, '/users/')
    })
}

function userDelete(uid,element){
    var decision = confirm("Are you sure ?");
    if (!decision) return

    $.ajax({
        'url': '/api/users/'+uid,
        'type': 'DELETE',
        'success': function(data){
            var tr  = $(element).parent().parent().prev()
            var dt  = $("#mytable").DataTable()
            var row = dt.row(tr)
            row.remove().draw()
            alert(data)
        }
    })
}

function toggleActive(uid, element) {
    var self = $(element).parent()
    var dt   = $("#mytable").DataTable()
    var cell = dt.cell(self)
    var decision = confirm('Do you want change validation?')
    if(!decision) return
    var val = !cell.data()

    $.ajax({
            'url'     : '/api/users/'+uid,
            'type'    : 'PATCH',
            'data'    : 'isEnabled='+val,
            'success' : function(response) {
                        alert(response);
                        cell.data(val).draw()
            },
            'error'   : function(xhr, status, error) {
                        alert(xhr.status + ": " + xhr.responseText)
            }
        })
}
