""" pysrv view point
----------------------------------------------------------------------
web-server dashboard view
"""

import pysrv.views.users
import pysrv.views.groups
import pysrv.views.access

import flask

from pysrv import dash


@dash.route('/')
def get_home():
    return flask.render_template(
        'dash.djhtml',
        header = 'dash',
    )
