#!/usr/bin/env python3

import flask

from pysrv import dash


@dash.route('/list')
def get_list():
    return flask.render_template(
        'base-table.djhtml',
        header  =   'User List',
        api     =   '/api/list'
    )


@dash.route('/logs')
def get_logs():
    return flask.render_template(
        'base-table.djhtml',
        header  =   'Access Log',
        api     =   '/api/logs',
        script  =   'js/access.js'
    )
