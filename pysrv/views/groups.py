#!/usr/bin/env python3

import flask

import pysrv
from pysrv import dash

import pysrv.db.passwd as db_passwd


@dash.route('/groups')
def get_groups_lst():
    return flask.render_template(
        'base-table.djhtml',
        header = 'groups',
        api    = '/api/groups',
        script = 'js/groups.js'
    )


@dash.route('/groups/<int:gid>')
def get_group(gid):
    instance = pysrv.session.query(
        db_passwd.Groups
    ).filter(
        db_passwd.Groups.gid == gid
    ).first()

    info = instance.__dict__
    if flask.request.args.get('xhr') == "True":
        return flask.render_template(
            'groupDetails.djhtml',
            **info,

        )

    return flask.render_template(
        'groupForm.djhtml',
        **info,
        header = 'Edit Group Information',
        api = '/api/groups/%d' % gid,
        method = 'PATCH'
    )


@dash.route('/groups/add')
def get_group_form():
    return flask.render_template(
        'groupForm.djhtml',
        header = 'Add Group',
        api    = '/api/groups/add'
    )
