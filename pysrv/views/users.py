#!/usr/bin/env python3

import flask

import pysrv
from pysrv import dash

import pysrv.db.passwd as db_passwd


@dash.route('/users')
def get_users_lst():
    return flask.render_template(
        'base-table.djhtml',
        header = 'users',
        api    = '/api/users',
        script = 'js/users.js'
    )


@dash.route('/users/<int:uid>')
def get_user(uid):
    instance = pysrv.session.query(
        db_passwd.Users.uid,
        db_passwd.Users.uname,
        db_passwd.Users.fullname.label('fullname'),
        db_passwd.Users.card,
        db_passwd.Users.lastIn,
        db_passwd.Users.createdOn,
        db_passwd.Users.updatedOn,
        db_passwd.Groups.gname.label('groups'),
        db_passwd.Groups.gid,
        db_passwd.Users.isEnabled,
        db_passwd.Users.validity
    ).filter(
        db_passwd.Users.uid == uid,
        db_passwd.Users.groups == db_passwd.Groups.gid
    ).first()

    groups = pysrv.session.query(
        db_passwd.Groups.gid,
        db_passwd.Groups.gname
    )

    info = dict (zip(instance.keys(), instance))
    if flask.request.args.get('xhr') == "True":
        return flask.render_template(
            'userDetails_xhr.djhtml',
            **info,

        )

    return flask.render_template(
        'userForm.djhtml',
        **info,
        header = 'Edit User Information',
        api    = '/api/users/%d'%uid,
        group  = groups,
        method = 'PATCH'
    )


@dash.route('/users/add')
def get_user_form():
    groups = pysrv.session.query(
            db_passwd.Groups.gid,
            db_passwd.Groups.gname
    )

    return flask.render_template(
        'userForm.djhtml',
        header = 'Add User',
        api    = '/api/users/add',
        group  = groups,
        gid    = 100
    )
