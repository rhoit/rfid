# CORE
# RPi.GPIO

# FLASK
flask
# flask-cors

# database
sqlalchemy

# DEPLOYMENT
gunicorn
