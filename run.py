#!/usr/bin/env python3

""" run-server
----------------------------------------------------------------------
switch the services, and direct deployable runner
"""

__version__  = 0.1
__PKG_ID__   = "3r"
__PKG_NAME__ = "run"
__PKG_DESC__ = "services switches"


import argparse

import pysrv

import multiprocessing
import gunicorn.app.base


class Unicorn(gunicorn.app.base.BaseApplication):
    def __init__(self, app, workers):
        self.workers = workers

        super().__init__()
        self.application = pysrv.app


    def load_config(self):
        self.cfg.set('bind', '0.0.0.0:8080')
        self.cfg.set('workers', self.workers)


    def load(self):
        return self.application


def main():
    ap = argparse.ArgumentParser(
        prog        = __PKG_NAME__,
        description = __PKG_DESC__
    )

    ap.add_argument(
        '-v',
        "--version",
        action  = "version",
        version = 'nepseguide %(prog)s v' + str(__version__))

    ap.add_argument(
        '-d',
        "--debug",
        action  = "store_true",
        default = False,
        help    = "debug mode")

    ap.add_argument(
        '-w',
        "--workers",
        type    = int,
        default = multiprocessing.cpu_count()*2,
        help    = "specify number of workers")

    opts, keys = ap.parse_known_args()

    pysrv.app.register_blueprint(pysrv.api, url_prefix='/api')
    pysrv.app.register_blueprint(pysrv.dash)

    if opts.debug:
        pysrv.app.run(debug=opts.debug, host='0.0.0.0')
        return

    pysrv.app.config['JSONIFY_PRETTYPRINT_REGULAR'] = False
    Unicorn(pysrv.app, opts.workers).run()


if __name__ == '__main__':
        main()
